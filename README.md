# Python Data Pipeline

## Presentation

1. Python et Data Engineering

Ce repository contient les réponses pour la première partie du test, intitulée "Python et Data Engineering" :
- Dans ``` doc/choix_et_approches.pdf ```, vous trouverez des éléments qui justifient mon approche et mes choix.
- La partie "Pour aller plus loin" est disponible dans ``` doc/Partie_1_pour_aller_plus_loin.pdf ```.

2. SQL

Le rendu pour la SQL est disponible dans ``` doc/Partie_2_SQL.pdf ```.

## 📁 Sructure

```
.
+- config                ⬅ Contient différents fichiers de configuration pour l'execution de la pipeline
+- doc                   ⬅ Contient la documentation du projet et notamment les réponses aux questions ouvertes
+- data                  ⬅ Contient les différents jeux de données ingérés et exportés par la pipeline
|  +- input              ⬅ Contient les fichiers ingérés
|  +- output             ⬅ Contient les fichiers crées
+- src                   ⬅ Contient les scripts du projet
|  +- jobs               ⬅ Contient les jobs python, c'est-à-dire les différentes steps de la pipeline
|  +- utils              ⬅ Contient les fonctionnalités python additionnelles
+- tests                 ⬅ Contient les scripts de test du projet
+- notebooks             ⬅ Contient le notebook permettant de répondre à la question 4_traitement_ad_hoc
+- main.py               ⬅ Le fichier lanceur de la pipeline
+- .gitlab-ci.yml        ⬅ la chaîne d'intégration continue du projet, qui vérifie que PEP8 est respecté et execute les tests
```

## How to launch

### Init

1. Clone the repository

``` git clone https://gitlab.com/sam.gout/data-pipeline-python.git ```

``` cd data-pipeline-python ```

2. Create a virtual environment and install requirements

Manually
``` python3 -m venv venv ```

``` . ./venv/bin/activate ```

``` pip install -r requirements.txt ```

Or by using the Makefile
``` make ```

### Launch

Each pipeline is executed with a predefined configuration. A configuration consists of a yaml file describing the workflow (the different ordered steps and their parameters).

To launch the data pipeline with the *pipeline_1.yaml* configuration in *run* mode, type :

``` python main.py pipeline_1.yaml --mode run ```

To see which arguments are available, type :

``` python main.py --help ```



