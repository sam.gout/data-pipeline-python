from src.jobs.datasource.read_json_files import read_json_files
from src.utils.df_test_case import DFTestCase
from unittest.mock import patch


class TestReadJsonFiles(DFTestCase):

    @patch("src.jobs.datasource.read_json_files.loads_json_with_regex")
    def test_read_json_file(self, mock_loads_json_with_regex):

        # prepare

        df_1 = self.read_csv("""col1
            1
            2
            3
            4
            """)

        df_2 = self.read_csv("""col1;type
            1;test
            2;test
            3;test
            4;test
            """)

        config = \
            [
                {
                    "open": {},
                    "name_regexs": [],
                    "name": "test",
                    "map_columns": {},
                    "astype": {}
                }
            ]

        mock_loads_json_with_regex.return_value = df_1

        # act

        returned_df = read_json_files(config=config)

        # assert

        self.assertEqualDF(returned_df, df_2)
