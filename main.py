from src.utils.pipeline_builder import PipelineBuilder
import argparse
from src.utils.tools import join_paths
from pprint import pprint
import logging

CONFIG_FOLDER_PATH = "config/"


def parse_arguments():
    '''
    This function parse arguments when calling ``main.py`` using bash.

    Returns
    -------
    mode: str
        Whether execute in ``step`` mode or in ``run`` mode.
    config: str
        The path of the pipeline config file.
    '''
    parser = argparse.ArgumentParser(
        description='Execute or print data pipelines \
        according to configuration files.'
    )

    parser.add_argument(
        'config', type=str,
        help='The path of the pipeline config file.')

    parser.add_argument(
        '-m', '--mode',
        choices=["step", "run"],
        help='Whether execute in step mode or in run mode.')

    args = parser.parse_args()

    mode = args.mode
    config_path = args.config

    return mode, config_path


def main(mode, config_path):
    '''
    This function create, build and print or run a pipeline according
    to the ``mode`` of launch and the ``config_path``
    of the configuration file.

    Parameters
    ----------
    mode: str
        Whether execute in ``step`` mode or in ``run`` mode.
    config: str
        The path of the pipeline config file.
    '''
    pipe = PipelineBuilder()

    pipe.build(join_paths("/", CONFIG_FOLDER_PATH, config_path))

    if mode == "step":
        pprint(pipe.print_steps())
    elif mode == "run":
        pipe.run()


if __name__ == '__main__':

    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s | %(levelname)s | %(message)s'
    )

    main(*parse_arguments())
