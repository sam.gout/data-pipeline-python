import json
import re
import yaml
from pathlib import Path
from src.utils.regexs import RegexTranslator


def load_config(path: str) -> list:
    # https://stackoverflow.com/questions/1773805/how-can-i-parse-a-yaml-file-in-python
    """
    Loads a dictionary from configuration file.

    Parameters
    ----------
    path: str
        The path of the YAML file to load the configuration from.

    Returns
    -------
    config : list(dict)
        The configuration list of dictionary loaded from the file.
    """

    with open(path, "r") as stream:
        config = yaml.safe_load(stream)

    return config


def multiple_regexs_sub(regexs: dict, doc: str):
    """
    Apply several regexs substitution stored in ``regexs`` to ``doc``.

    Parameters
    ----------
    regexs: dict
        The regexs pattern and replacement as key-value of a dictionary.
    doc: str
        The document on which apply the substitution.

    Returns
    -------
    doc : str
        The cleaned up document.
    """

    for pattern, replace in regexs.items():

        doc = re.sub(pattern, replace, doc)

    return doc


def loads_json_with_regex(open_config: dict, name_regexs: list):
    """
    Load a json file. Allow the possibility to apply regexs on the
    file before parsing it as JSON file.

    Parameters
    ----------
    open_config: dict
        The parameter for opening the file.
    name_regexs: list
        The names of the regexs to apply to the file before parsing it.

    Returns
    -------
    loaded_file:
        The deserialized file as python object according the JSONDecoder
        conversion table.
    """

    with open(**open_config) as file:
        read_file = file.read()

    # retrieve regexs by name

    regexs = RegexTranslator().get_regexs(name_regexs)

    # fixing the file by applying regexs

    fixed_file = multiple_regexs_sub(regexs, read_file)

    # loading the file as json

    loaded_file = json.loads(fixed_file)

    return loaded_file


def join_paths(sep: str, *args):
    """
    Join several strings with a separator ``sep``.

    Parameters
    ----------
    sep: str
        The separator used for the join.
    *args: list(str)
        The strings to join.

    Returns
    -------
    The joined string.
    """
    return sep.join(args)


def get_module_path(root, module_name):
    """
    Search for a function in a directory tree.

    This function get the path of a python function with its name
    in a directory tree before translating the path to
    a module search path.

    Parameters
    ----------
    root: str
        The path of the root directory.
    module_name: str
        The name of the module we are looking for.

    Returns
    -------
    The search path of the desired module.
    """

    for path in Path(root).rglob(module_name+'.py'):

        module_path = str(path.parent.joinpath(path.stem))

    return module_path.replace("/", ".")
