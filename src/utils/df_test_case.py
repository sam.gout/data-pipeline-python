import unittest
import pandas as pd
from io import StringIO


class DFTestCase(unittest.TestCase):
    """
    A wrapper for unittest.TestCase that make easier to produce
    unit test for pandas.Dataframe based function.
    ...

    Methods
    -------
    read_csv(csv_text)
        make easier the read of inline csv.
    assertEqualDF(csv_text)
        make easier the assertion of two dataframes.
    """

    def read_csv(self, csv_text: str):
        """Load an inline csv into a pandas.DataFrame

        Parameters
        ----------
        csv_text : str
            The inline csv.
        Returns
        -------
        The pandas.DataFrame
        """

        return pd.read_csv(StringIO(csv_text), sep=";")

    def assertEqualDF(self, df1: pd.DataFrame, df2: pd.DataFrame):
        """Check the equality of two dataframes

        Parameters
        ----------
        df1: pd.DataFrame
            The first dataframe.
        df2: pd.DataFrame
            The second dataframe.

        Raises
        ------
        AssertionError
            If two dataframes are not equal.
        """

        return pd._testing.assert_frame_equal(df1, df2, check_dtype=False)
