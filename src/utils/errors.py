

class MandatoryColumns(Exception):
    """Generic exception for mylib"""
    def __init__(self, msg, original_exception):

        super(MandatoryColumns, self)\
            .__init__(msg + (": %s" % original_exception))

        self.original_exception = original_exception
