import numpy as np


class RegexTranslator(object):
    """
    A class used to store regexs by their names.
    ...

    Attributes
    ----------
    regexs_sub : dict
        A dictionary containing the name and the regexs as key-value.

    Methods
    -------
    get_regexs(names)
        Retrieve regexs by their names.
    """

    def __init__(self):

        self.regexs_sub = \
            {
                "remove_literal_hexa": (r'\\x[0-9a-fA-F]+', ''),
                "replace_empty_str_by_nan": (r'^\s*$', np.nan),
                "remove_trailing_comma": (r',[ \t\r\n]+\]', ']'),
            }

    def get_regexs(self, names: list):
        """
        Get the desired regexs according to their names.

        Parameters
        ----------
        names : list
            The name of the regexs needed.

        Returns
        -------
        A dictionary containing the regexs pattern and replacement
        as key-value.
        """

        return dict([self.regexs_sub[name] for name in names])
