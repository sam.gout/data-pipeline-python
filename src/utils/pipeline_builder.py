import importlib
from genpipes import compose
from src.utils.tools import load_config, get_module_path
import inspect
import logging
logger = logging.getLogger(__name__)


class PipelineBuilder(object):
    """
    A class used to build and run pipelines using genpipes.

    ...

    Attributes
    ----------
    steps : list(tuple)
        A list containing the steps of ``pipe``
    pipe : genpipes.compose.Pipeline
        A genpipes pipeline

    Methods
    -------
    build(config_path)
        Build a pipeline

    _add_step(name, config)
        Add a step to ``pipe``

    run
        Run ``pipe``

    print_steps
        Print the steps of ``pipe``
    """

    def __init__(self):
        """
        Parameters
        ----------
        """

        self.steps = []  # init the steps of the pipeline

    def build(self, config_path: str):
        """Build a pipeline according to configurations.

        Parameters
        ----------
        config_path : str
            The path of a configuration file to load and to follow.
            The configuration files are usually YAML files, and are stored
            in the config folder.
        """

        steps = load_config(config_path)

        for step in steps:

            step_name = step["step_name"]
            del step["step_name"]

            step_config = step

            self._add_step(name=step_name, config=step_config)

        self.pipe = compose.Pipeline(steps=self.steps)

        logger.info(
            "A pipeline of %s steps has been built",
            len(self.steps)
        )

        return self

    def _add_step(self, name: str, config: dict):
        """Add a step to the pipeline according to his name and his config

        Parameters
        ----------
        name : str
            The name of the step to add to the pipe.
            This refers to the name of the python function to call.

        config : dict
            The configuration of the step
        """

        # import the job

        step_path = get_module_path("src", name)

        step_job = getattr(importlib.import_module(step_path), name)

        # define the step description

        step_description = inspect.getdoc(step_job)

        # set the job configuration

        step_config = config

        # append the step

        self.steps.append(
            (
                step_description,
                step_job,
                step_config,
            )
        )

        return self

    def run(self):
        """Run the pipeline"""

        logger.info("Beginning of the run")

        data = self.pipe.run()

        logger.info("End of the run")

        return data

    def print_steps(self):
        """Print the pipeline"""

        return self.pipe.steps
