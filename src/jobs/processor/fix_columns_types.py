import pandas as pd
from genpipes import declare
from typing import Iterator
import logging
logger = logging.getLogger(__name__)


@declare.processor()
def fix_columns_types(stream: Iterator[pd.DataFrame], columns_types: dict):
    """
    Set new columns types for dataframes of a stream.

    This function set news columns types for dataframes from a generator
    according to ``columns_types``.

    Parameters
    ----------
    stream : Iterator[pd.DataFrame]
        A generator that represent the stream of dataframes.
    columns_types : dict
        A mapping between a column name and its pandas type.

    Returns
    -------
    df : Iterator[pd.DataFrame]
        The stream of dataframes after typing.

    Examples
    --------

    """
    for df in stream:

        logger.info(' .. STEP fix_columns_types')

        df_typed = df.astype(columns_types)

        yield df_typed
