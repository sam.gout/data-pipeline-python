import pandas as pd
from genpipes import declare
from typing import Iterator, List
from src.utils.regexs import RegexTranslator
import logging
logger = logging.getLogger(__name__)


@declare.processor()
def clean_with_regexs(stream: Iterator[pd.DataFrame], name_regexs: List[str]):
    """
    Apply regexs to a stream of dataframes.

    This function retrieve some regexs by their names before applying them
    to a generator of dataframes.

    Parameters
    ----------
    stream : Iterator[pd.DataFrame]
        A generator that represent the stream of dataframes.
    name_regexs : List[str]
        The names of the regexs to apply on the stream.

    Returns
    -------
    df : Iterator[pd.DataFrame]
        The stream of the processed dataframes.

    Examples
    --------

    """
    for df in stream:

        logger.info(' .. STEP clean_with_regexs')

        # retrieve the regex by name

        regexs = RegexTranslator().get_regexs(name_regexs)

        # perform the remplacement

        df.replace(regexs, regex=True, inplace=True)

        yield df
