import pandas as pd
from genpipes import declare
from typing import Iterator
import logging
logger = logging.getLogger(__name__)


@declare.processor()
def normalize_id_column(stream: Iterator[pd.DataFrame]):
    """
    Normalize the id column of dataframes of a stream.

    This function remove and then set a new unique id column to dataframes.

    Parameters
    ----------
    stream : Iterator[pd.DataFrame]
        A generator that represent the stream of dataframes.

    Returns
    -------
    df : Iterator[pd.DataFrame]
        The stream of the dataframes with a new id column.

    Examples
    --------

    """
    for df in stream:

        logger.info(' .. STEP normalize_id_column')

        df_with_id = df\
            .reset_index()\
            .drop(columns='id')\
            .rename(columns={"index": "id"})

        df_with_id['id'] = df_with_id['id'] + 1

        yield df_with_id
