import pandas as pd
from genpipes import declare
from typing import Iterator, Callable, List
from src.jobs.datasource.read_csv_files import read_csv_files
import logging
logger = logging.getLogger(__name__)


@declare.processor(inputs=[read_csv_files])
def get_drugs_mentions_articles(
    stream: Iterator[pd.DataFrame],
    read_csv_files: Callable,
    drugs_files: List[dict]
):
    """
    Compute new dataframes containing the mentions of a drug in an article
    from a stream.

    This function compute new dataframes whose rows correspond to the
    mention of a drug in an article. The final data frames contain
    as many rows as the number of mentions to a drug contained in
    the drugs dataframes.

    Parameters
    ----------
    stream : Iterator[pd.DataFrame]
        A generator that represent the stream of dataframes.
    read_csv_files : Callable
        The ``read_csv_files`` function from src.jobs.datasource
    drugs_files : List[dict]
        The configuration to be passed to ``read_csv_files``.
        Contains the configuration of each drugs files to read.

    Returns
    -------
    df : Iterator[pd.DataFrame]
        The stream of the computed dataframes.

    Examples
    --------

    """

    logger.info(' .. STEP get_drugs_mentions_articles')

    # get the drugs dataframe

    drugs = read_csv_files(config=drugs_files)

    # compute a new dataframe whose rows correspond to a mention
    # of a drug in a article

    for df in stream:

        li = [
            df[
                df['title'].str
                .upper().str
                .contains(drug)
            ]
            .assign(drug=drug)
            for drug in drugs.drug
        ]

        df_filtered = pd.concat(li)

    # merge with the drugs dataframe to add the atccode column

        df_filtered = pd.merge(
            drugs[["atccode", "drug"]],
            df_filtered,
            how="left",  # left join to keep also consider the unfound drugs
            on=["drug"]
        )

        yield df_filtered
