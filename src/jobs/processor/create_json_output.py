import pandas as pd
from genpipes import declare
from typing import Iterator
import json
import logging
logger = logging.getLogger(__name__)


@declare.processor()
def create_json_output(stream: Iterator[pd.DataFrame], output_name: str):
    """
    Create a json representation of dataframes from a stream and save them
    locally.

    Parameters
    ----------
    stream : Iterator[pd.DataFrame]
        A generator that represent the stream of dataframes.
    output_name : str
        The path to save the json representation.

    Returns
    -------
    df : Iterator[pd.DataFrame]
        The stream of the dataframes.

    Examples
    --------

    """

    for df in stream:

        logger.info(' .. STEP create_json_output')

        # convert the date column back to an object type to be readable in json

        df['date'] = df['date'].dt.strftime('%Y-%d-%m')

        # see https://stackoverflow.com/questions/53603857/
        # pandas-to-json-changes-date-format-to-epoch-time

        # create the json string

        json_string = df.groupby(['atccode', 'drug'])\
            .apply(lambda x: x[['date', 'id', 'title', 'type', 'journal']]
                   .dropna()
                   .to_dict('records'))\
            .reset_index()\
            .rename(columns={0: 'mentions'})\
            .to_json(orient='records')

        # save the results on a json file

        with open(output_name, 'w', encoding='utf8') as outfile:
            try:
                json.dump(
                    json.loads(json_string),
                    outfile,
                    indent=2,
                    sort_keys=True,
                    ensure_ascii=False
                )

                logger.info(
                    " .... Pipeline outputs has been saved in %s",
                    output_name
                )

            except Exception as exc:
                raise exc

        yield df
