import pandas as pd
from genpipes import declare
from typing import Iterator
import logging
logger = logging.getLogger(__name__)


@declare.processor()
def combine_duplicate_and_remove_na(stream: Iterator[pd.DataFrame]):
    """
    Combine duplicate rows and remove NA rows from a stream.

    This function group a generator of dataframe by a unique key
    composed of ``title`` and ``type`` and then remove NA rows and
    combine the attributes of rows that have the same key.

    Parameters
    ----------
    stream : Iterator[pd.DataFrame]
        A generator that represent the stream of dataframes.

    Returns
    -------
    df : Iterator[pd.DataFrame]
        The stream of the processed dataframes.

    Examples
    --------

    """
    for df in stream:

        logger.info(' .. STEP combine_duplicate_and_remove_na')

        df_nodup = df.groupby(['title', 'type'], as_index=False).first()

        yield df_nodup
