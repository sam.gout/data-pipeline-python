import pandas as pd
from genpipes import declare


@declare.datasource()
def read_csv_files(config: list) -> pd.DataFrame:
    '''
    Load csv files as pandas dataframes according to configurations
    and merge them.

    Parameters
    ----------
    config : list of dict
        The configuration of each file to read.

    Returns
    -------
    df : pandas.DataFrame
        The merged dataframe.
    '''

    # custom_date_parser = lambda x: datetime.strptime(x, "%d %B %Y")

    df = pd.concat(
        (
            pd.read_csv(**f['read_csv'])
            .assign(type=f['name'])
            .rename(columns=f['map_columns'])
            for f in config
        )
    )

    return df
