import pandas as pd
from genpipes import declare
from src.utils.tools import loads_json_with_regex


@declare.datasource()
def read_json_files(config: list) -> pd.DataFrame:
    '''
    Load json files as pandas dataframes.

    This function load and merge json files as pandas dataframe
    according to configurations. It also allows the possibility
    to apply regexs substitution on the files before parsing them.

    Parameters
    ----------
    config : list of dict
        The configuration of each file to read.

    Returns
    -------
    df : pandas.DataFrame
        The merged dataframe.
    '''

    df = pd.concat(
        (
            pd.DataFrame(
                loads_json_with_regex(
                    open_config=f['open'],
                    name_regexs=f['name_regexs']
                )
            )
            .assign(type=f['name'])
            .rename(columns=f['map_columns'])
            for f in config
        )
    )

    return df
