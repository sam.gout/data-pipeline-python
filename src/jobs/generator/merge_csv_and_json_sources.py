import pandas as pd
from genpipes import declare
from typing import Callable
from src.jobs.datasource.read_csv_files import read_csv_files
from src.jobs.datasource.read_json_files import read_json_files
from src.utils.errors import MandatoryColumns
import logging
logger = logging.getLogger(__name__)


@declare.generator(inputs=[read_csv_files, read_json_files])
def merge_csv_and_json_sources(
    read_csv_files: Callable,
    read_json_files: Callable,
    csv_files: list,
    json_files: list
) -> pd.DataFrame:
    """
    Merge two sources of data.

    This function call ``read_csv_files`` and ``read_json_files``
    before concatenate their output.

    Parameters
    ----------
    read_csv_files : Callable
        The ``read_csv_files`` function from src.jobs.datasource
    read_json_files : Callable
        The ``read_json_files`` function from src.jobs.datasource
    csv_files : list of dict
        The configuration to be passed to ``read_csv_files``
    json_files : list of dict
        The configuration to be passed to ``read_json_files``

    Raises
    ------
    MandatoryColumnsError
        If the merged dataframe does not contains all the expected columns.

    Returns
    -------
    df : pandas.DataFrame
        The merged dataframe.
    """
    logger.info(' .. STEP merge_csv_and_json_sources')

    df1 = read_csv_files(config=csv_files)
    df2 = read_json_files(config=json_files)

    df = pd.concat([df1, df2])  # concat the two sources of data

    try:
        # only keep the relevant columns
        df = df[["id", "journal", "title", "date", "type"]]
    except KeyError as exc:
        raise MandatoryColumns(
            "journal, title, date, id are mandatory columns.",
            exc
        )

    return df
