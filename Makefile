init:
	python3 -m venv venv
	. ./venv/bin/activate
	pip install -r requirements.txt

run:
	python main.py pipeline_1.yaml --mode step
